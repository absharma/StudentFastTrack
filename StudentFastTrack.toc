\contentsline {section}{\numberline {1}Infrastructure}{3}{section.1}
\contentsline {section}{\numberline {2}Basic software installations}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Installing MacPorts}{3}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Python}{3}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}PyROOT}{3}{subsubsection.2.1.2}
\contentsline {subsection}{\numberline {2.2}ROOT installation}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}XQuartz}{4}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Mac Command Line Tools \& Xcode}{4}{subsection.2.4}
\contentsline {section}{\numberline {3}Basic Linux Commands}{4}{section.3}
\contentsline {section}{\numberline {4}LXPLUS}{6}{section.4}
\contentsline {section}{\numberline {5}Screen for Linux}{6}{section.5}
\contentsline {section}{\numberline {6}PuTTY}{6}{section.6}
\contentsline {section}{\numberline {7}Using FileZilla for easy file transfers}{7}{section.7}
\contentsline {section}{\numberline {8}Source code management using Gitlab}{7}{section.8}
\contentsline {subsection}{\numberline {8.1}Git global setup}{8}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Create a new repository}{8}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}Existing folder}{8}{subsection.8.3}
\contentsline {subsection}{\numberline {8.4}Existing Git repository}{8}{subsection.8.4}
\contentsline {section}{\numberline {9}Documenting in \LaTeX }{8}{section.9}
\contentsline {section}{\numberline {10}Remote Communication}{8}{section.10}
\contentsline {subsection}{\numberline {10.1}RS232 via PySerial}{8}{subsection.10.1}
\contentsline {section}{\numberline {11}Using the XIMEA microscopy camera}{8}{section.11}
\contentsline {section}{\numberline {12}Exercises}{9}{section.12}
\contentsline {subsection}{\numberline {12.1}Create a power supply python class definition}{9}{subsection.12.1}
\contentsline {subsection}{\numberline {12.2}Current vs Voltage scan (with signal handler) \& Save to file}{10}{subsection.12.2}
\contentsline {subsection}{\numberline {12.3}Plot the above IV scan in PyROOT}{10}{subsection.12.3}
\contentsline {subsection}{\numberline {12.4}Integrate scan results plot with Elog \& submit IV plot to Elog}{10}{subsection.12.4}
\contentsline {subsection}{\numberline {12.5}Remote access via SSH and Screen}{10}{subsection.12.5}
\contentsline {subsection}{\numberline {12.6}setup.sh file preparation}{10}{subsection.12.6}
\contentsline {subsection}{\numberline {12.7}Archive code onto a Gitlab repository}{11}{subsection.12.7}
\contentsline {subsection}{\numberline {12.8}CMAKE package preparation}{11}{subsection.12.8}
