\documentclass[11pt,a4paper,notitlepage]{article}
\usepackage{authblk}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{cite}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage[top=2cm, bottom=2cm, left=2cm, right=2cm]{geometry}
\usepackage{units}
\usepackage{enumitem}
\usepackage{verbatim}
\usepackage{lineno}
\usepackage{hyperref}
\usepackage{blindtext}
\usepackage{listings}
%\linenumbers

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Definitions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\tfmunits}{K$\cdot$cm$^\text{2}$/W\,}
\newcommand{\heatflux}{W/cm$^\text{2}\,$}
\newcommand{\degrees}{$^\circ\,$}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Title
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\date{\small \today}

\title{EP-ADE-ID \\ \ \\Student Fast-Track}
\author[a,b]{Abhishek Sharma}
\author[a]{Carlos Solans}

\affil[a]{CERN}
\affil[b]{University of Oxford}


\begin{document}
\maketitle

\abstract{This document aims to provide an approachable introduction to the various tools and methods likely to be encountered by a new applied physics student joining the EP-ADE-ID team along with practical information to get started. An additional aim of this document is to encourage a standardised approach in software development, to enhance its legibility, portability \& longevity.
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Body
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newpage
\tableofcontents
\newpage

 


\section{Infrastructure}
\label{sec:Introduction}
Most laboratory work is conducted in one of 3 areas in building 161: Laboratories 23, 24 and workarea in room 009. The layout showing key instruments in each area are shown on figure \ref{Labs}.

\begin{figure}[h]
\includegraphics[width=12cm]{figures/Lab24.png}
\includegraphics[width=12cm]{figures/Lab23left.png}
\includegraphics[width=12cm]{figures/Lab23right.png}
\includegraphics[width=12cm]{figures/Workarea009.png}
\centering
\caption{Laboratory views shown (from top to bottom), starting with Lab24, Lab23 left view upon entry and right view and finally the workarea in room 161/1-009.}
\label{Labs}
\end{figure}

One key repository of information relating to the work of the entire group is the pixel group website: \textit{www.ade-pixel-group.web.cern.ch}, a screenshot of which is shown on figure \ref{groupWebsite}.

\begin{figure}[h]
\includegraphics[width=12cm]{figures/ade-pixel-group-website.png}
\centering
\caption{The group website contains aggregated information on the entire team's work and is a good source for reference material: \textit{www.ade-pixel-group.web.cern.ch}}
\label{groupWebsite}
\end{figure}


\section{Basic software installations}
\subsection{Installing MacPorts}
Macports is a command-line installer assistant which drastically facilitates the installation of most required packages. Install Macports from \href{https://guide.macports.org/chunked/installing.macports.html}{here}.
\subsubsection{Python}
Python 2.7 is used preferencially over Python 3, for its comprehensive pre-existing set of compatible packages. After installing MacPorts, execute the following commands to install python 2.7:
\begin{itemize}
\item \textit{sudo port selfupdate}
\item \textit{sudo port install python27}

\item Make this Python installation the default:\\\textit{sudo port select --set python python27}
\end{itemize}
\subsubsection{PyROOT}
\label{sec:PyRoot}
Installation command:
\begin{itemize}
\item \textit{sudo port install root6 -python27}
\\ \\Make this ROOT and Python installation the default:
\item \textit{sudo port select --set root root6}
\item \textit{sudo port select --set python python27}
\end{itemize}

Set environment variables:
\begin{itemize}
\item \textit{export PATH=``\$PATH:/opt/local/bin:/opt/local/sbin"}
\item \textit{export PYTHONPATH=``\$PYTHONPATH:/opt/local/lib/root"}
\end{itemize}

\subsection{ROOT installation}
\label{sec:ROOT installation}

To install ROOT on a Mac, see section \ref{sec:PyRoot}.
\\
Installation instructions for alternate OS...

\subsection{XQuartz}
XQuartz allows for graphical interfaces to be displayed when remotely logged in to, say, an lxplus node (see section \ref{lxplus} for more details). XQuartz\cite{XQuartz} can be installed from \href{https://www.xquartz.org}{here}.

\subsection{Mac Command Line Tools \& Xcode}
For Mac users, it will be necessary to install Command Line Tools \& Xcode as they are a pre-requisit for the commonly used tools such as ROOT to be functional.
Xcode can be installed for free from the Mac App Store. Instructions on how to install Command Line Tools can be found \href{tobefilled}{here}.


\section{Basic Linux Commands}
Below is a list of commonly used Linux terminal commands taken directly from maker.pro\cite{Linux}

%\begin{comment}
\begin{enumerate}
\item \textit{pwd} — When you first open the terminal, you are in the home directory of your user. To know which directory you are in, you can use the \textit{pwd} command. It gives us the absolute path, which means the path that starts from the root. The root is the base of the Linux file system. It is denoted by a forward slash( / ). The user directory is usually something like \textit{/home/username}.

\item \textit{ls} — Use this command to know what files are in the directory you are in. You can see all the hidden files by using the command \textit{ls -a}.

\item \textit{cd} — Use the "cd" command to go to a directory. For example, if you are in the home folder, and you want to go to the downloads folder, then you can type in \textit{cd Downloads}. Remember, this command is case sensitive, and you have to type in the name of the folder exactly as it is. But there is a problem with these commands. Imagine you have a folder named “Raspberry Pi”. In this case, when you type in “cd Raspberry Pi”, the shell will take the second argument of the command as a different one, so you will get an error saying that the directory does not exist. Here, you can use a backward slash. That is, you can use \textit{cd Raspberry\ Pi} in this case. Spaces are denoted like this: If you just type \textit{cd} and press enter, it takes you to the home directory. To go back from a folder to the folder before that, you can type “cd ..” . The two dots represent back.

\item \textit{mkdir} — Use the mkdir command when you need to create a folder or a directory. For example, if you want to make a directory called “DIY”, then you can type “mkdir DIY”. Remember, as told before, if you want to create a directory named “DIY Hacking”, then you can type “mkdir DIY\ Hacking”.

\item \textit{rm} — Use the rm command to delete files and directories. But rm cannot simply delete a directory. Use “rm -r” to delete a directory. In this case, it deletes both the folder and the files in it.

\item \textit{touch} — The touch command is used to create a file. It can be anything, from an empty txt file to an empty zip file. For example, \textit{touch new.txt}.

\item \textit{man} \& \textit{--help} — To know more about a command and how to use it, use the man command. It shows the manual pages of the command. For example, \textit{man cd} shows the manual pages of the cd command. Typing in the command name and the argument helps it show which ways the command can be used (e.g., \textit{cd –help}).

\item \textit{cp} — Use the cp command to copy files through the command line. It takes two arguments: The first is the location of the file to be copied, the second is where to copy.

\item \textit{mv} — Use the mv command to move files through the command line. We can also use the mv command to rename a file. For example, if we want to rename the file text” to “new”, we can use \textit{mv text new}. It takes the two arguments, just like the cp command.

\item \textit{locate} — The locate command is used to locate a file in a Linux system, just like the search command in Windows. This command is useful when you don't know where a file is saved or the actual name of the file. Using the -i argument with the command helps to ignore the case (it doesn't matter if it is uppercase or lowercase). So, if you want a file that has the word “hello”, it gives the list of all the files in your Linux system containing the word "hello" when you type in \textit{locate -i hello}. If you remember two words, you can separate them using an asterisk (*). For example, to locate a file containing the words "hello" and "this", you can use the command \textit{locate -i *hello*this}.

\item \textit{echo} — The "echo" command helps us move some data, usually text into a file. For example, if you want to create a new text file or add to an already made text file, you just need to type in, “echo hello, my name is alok >> new.txt”. You do not need to separate the spaces by using the backward slash here, because we put in two triangular brackets when we finish what we need to write.

\item \textit{cat} — Use the cat command to display the contents of a file. It is usually used to easily view programs.

\item \textit{sudo} — A widely used command in the Linux command line, sudo stands for "SuperUser Do". So, if you want any command to be done with administrative or root privileges, you can use the sudo command. You can enter the root command line using the command “sudo bash”, then type in your user password. You can also use the command “su” to do this, but you need to set a root password before that. For that, you can use the command “sudo passwd” (not misspelled, it is passwd). Then type in the new root password.

\item \textit{du} — Use du to know the disk usage of a file in your system. If you want to know the disk usage for a particular folder or file in Linux, you can type in the command df and the name of the folder or file. For example, if you want to know the disk space used by the documents folder in Linux, you can use the command \textit{du Documents}. You can also use the command \textit{ls -lah} to view the file sizes of all the files in a folder.

\item \textit{tar} — Use tar to work with tarballs (or files compressed in a tarball archive) in the Linux command line. It has a long list of uses. It can be used to compress and uncompress different types of tar archives like .tar, .tar.gz, .tar.bz2,etc. It works on the basis of the arguments given to it. For example, \textit{tar -cvf} for creating a .tar archive, -xvf to untar a tar archive, -tvf to list the contents of the archive, etc.

\item \textit{zip}, \textit{unzip} — Use zip to compress files into a zip archive, and unzip to extract files from a zip archive.

\item \textit{apt-get} — Use apt to work with packages in the Linux command line. Use apt-get to install packages. This requires root privileges, so use the sudo command with it. For example, if you want to install the text editor jed, we can type in the command \textit{sudo apt-get install jed}. Similarly, any packages can be installed like this. It is good to update your repository each time you try to install a new package. You can do that by typing \textit{sudo apt-get update}. You can upgrade the system by typing \textit{sudo apt-get upgrade}. We can also upgrade the distro by typing \textit{sudo apt-get dist-upgrade}. The command \textit{apt-cache search} is used to search for a package. If you want to search for one, you can type in \textit{apt-cache search jed} (this doesn't require root).

\item \textit{chmod} — Use chmod to make a file executable and to change the permissions granted to it in Linux. Imagine you have a python code named numbers.py in your computer. You'll need to run \textit{python numbers.py} every time you need to run it. Instead of that, when you make it executable, you'll just need to run \textit{numbers.py} in the terminal to run the file. To make a file executable, you can use the command \textit{chmod +x numbers.py} in this case. You can use \textit{chmod 755 numbers.py} to give it root permissions or \textit{sudo chmod +x numbers.py} for root executable.

\item \textit{hostname} — Use hostname to know your name in your host or network. Basically, it displays your hostname and IP address. Just typing \textit{hostname} gives the output. Typing in \textit{hostname -I} gives you your IP address in your network.

\end{enumerate}
%\end{comment}


\section{LXPLUS}
\label{lxplus}
CERN has a network of physical computers (or nodes) located in the IT department that all have a similar Linux installation configuration. This Linux computer cluster is known as LXPLUS. A user can remotely log into one of these computers from inside or outside of the CERN network using Secure Shell (a.k.a. SSH), allowing them the ability to use a linux environment already preset with the CERN defaults.

A user can log in remotely into an lxplus node by executing the following command in the terminal: \textit{ssh -XY username@lxplus.cern.ch}

One very useful feature of SSH-ing is the ability to log in to a specific physical computer in the laboratory. To achieve this, simply SSH directly into a specific computer using its name, e.g. \textit{ssh -XY username@pcatlidlas02} if within the CERN network, and SSH-ing twice if outside the CERN network, e.g.: \textit{ssh -XY username@lxplus.cern.ch} (entering a CERN local network node), and then \textit{ssh -XY username@pcatlidlas02}


\section{Screen for Linux}
Screen is a very useful tool to be used in complement with lxplus. It allows to resume the view of a terminal open in a physical pc in one of the laboratories and can be used execute measurements whilst off site from CERN or to keep track of ongoing processes and measurements. To install screen type the following in the terminal: \textit{yum install screen}

Common Screen commands are listed below:
\begin{itemize}
\item \textit{screen -S screenName} - use this command to create a new screen.
\item \textit{screen -ls} - displays the currently existing list of screens.
\item \textit{screen -d} - detach from the screen currently in.
\item \textit{screen -r} - re-attach to a specific screen.
\end{itemize}


\section{PuTTY}
\label{sec:PuTTY}
PuTTY\cite{Putty} is a compact Windows software that allows for command line-based remote access to lxplus nodes (see section \ref{lxplus} for more information on lxplus). The software can be downloaded from \href{https://putty.org}{here}. Figure \ref{Putty} shows the required configurations that need to be set.

\begin{figure}[h!]
\includegraphics[width=5cm]{figures/Putty1.png}%
\includegraphics[width=5cm]{figures/Putty2.png}%
\includegraphics[width=5cm]{figures/Putty3.png}
\includegraphics[width=5cm]{figures/Putty4.png}%
\includegraphics[width=5cm]{figures/Putty5.png}
\centering
\caption{PuTTY configuration.}
\label{Putty}
\end{figure}


\section{Using FileZilla for easy file transfers}
\label{sec:Filezilla}
FileZilla\cite{FileZilla} is a file transfer software that can be used in particular for file transfers between one's local computer disk and cern's lxplus file system (known as \textit{afs}). Download FileZilla from \href{https://filezilla-project.org}{here}. Figure \ref{FileZilla} shows the graphical interface of the software.

\begin{figure}[h!]
\includegraphics[width=12cm]{figures/FileZilla.png}
\centering
\caption{FileZilla view on OSX.}
\label{FileZilla}
\end{figure}

Establish a connection by supplying the following information: \textit{Host: lxplus.cern.ch}, one's CERN credentials for username and password and \textit{port: 22}. Files can then be transferred by simply dragging and dropping them from one disk to another.


\section{Source code management using Gitlab}
\label{sec:Source code management using Gitlab}

Gitlab is CERN's version of Github, a source code curation repository. Its use is encouraged as a backup for one's work as well as facilitating collaborative software development through version control.

Gitlab commands can be executed from the terminal. Below are a list of the basic commands needed to create a new repository and upload changes back onto Gitlab. The commands below use as an example accessing this very document from its Gitlab repository:

\subsection{Git global setup}
\begin{lstlisting}
git config --global user.name "Abhishek Sharma"
git config --global user.email "abhishek.sharma\@cern.ch"
\end{lstlisting}
\subsection{Create a new repository}
\begin{lstlisting}
git clone ssh://git@gitlab.cern.ch:7999/absharma/StudentFastTrack.git
cd StudentFastTrack
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
\end{lstlisting}
\subsection{Existing folder}
\begin{lstlisting}
cd existingfolder
git init
git remote add origin ssh://git@gitlab.cern.ch:7999/absharma/StudentFastTrack.git
git add .
git commit -m "Initial commit"
git push -u origin master
\end{lstlisting}
\subsection{Existing Git repository}
\begin{lstlisting}
cd existingRepo
git remote rename origin old-origin
git remote add origin ssh://git@gitlab.cern.ch:7999/absharma/StudentFastTrack.git
git push -u origin --all
git push -u origin --tags
\end{lstlisting}


\section{Documenting in \LaTeX}
\label{sec:Documenting in LaTeX}
All permanent documentation should be made in \LaTeX. This will ease the task of maintaining the document even if taken over by someone else. The \LaTeX\ source code of this document can be used as a template for any documentation you may wish to prepare. The source code can be found \href{https://:@gitlab.cern.ch:8443/absharma/StudentFastTrack.git}{here}\footnote{For instructions on how to download the files using Gitlab, see section \ref{sec:Source code management using Gitlab}.}.
If you are a summer student, it will be a requirement for you to submit a LaTeX report detailing the work conducted over your term at CERN.


\section{Remote Communication}
Remote communication with laboratory instruments such as power supplies and motion controllers, to name a few, allows for the automation of mundane operations like voltage ramping or repetitive current measurements. This also enables the user to time-efficiently perform fine measurement scans across a given range of values with the target instrument. A typical remote communication interface is known as RS232, detailed in section \ref{RS232 via PySerial} and exercises \ref{Power supply python class}.

\subsection{RS232 via PySerial}
\label{RS232 via PySerial}


\section{Using the XIMEA microscopy camera}
\label{sec:ximea}
Download the XIMEA camera software from \href{http://www.ximea.com/downloads/recent/XIMEA_OSX_SP.dmg}{here}.
Plug the USB cable into your mac and run the software.
Follow the instructions as shown on figure \ref{XimeaWindow} and the camera image should be displayed on screen.

\begin{figure}[h!]
\includegraphics[width=10cm]{figures/XimeaWindow2.png}
\centering
\caption{Ximea camera software interface.}
\label{XimeaWindow}
\end{figure}



\section{Exercises}
\subsection{Create a power supply python class definition}
\label{Power supply python class}
This exercise will teach you how to create a Python class definition for a Keithley 2410 series power supply to be used with PySerial via RS232 communication. This workflow can then be used to create similar class definitions for most RS232-capable laboratory instruments (they being the vast majority).

\begin{enumerate}
\item Google search for the Keithley 2410 user manual.
\item Identify the Commands / Communication section and obtain 
\end{enumerate}

\begin{lstlisting}
#!/usr/bin/env python
#############################################
# Keithley 2400 
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# February 2016
#############################################

from time import sleep
from SerialCom import SerialCom

class Keithley:
    sc = None
    def __init__(self,portname,_baudrate=9600):
        self.sc = SerialCom(portname,timeout=1)
    def setCurrentLimit(self,fvalAmps):
        self.sc.write(":SENSE:CURR:PROT %f"%fvalAmps)
    def setVoltageLimit(self,fvalVolts):
        self.sc.write(":SENSE:VOLT:PROT %f"%fvalVolts)
    def setCurrent(self,fsetValAmps):
        self.sc.write(":SOUR:CURR %f"%fsetValAmps)
    def setVoltage(self,fsetValVolts):
        self.sc.write(":SOUR:VOLT %f"%fsetValVolts)
    def enableOutput(self,bEnable):
        if bEnable == True:
            self.sc.write(":OUTPUT ON")
        elif bEnable == False:
            self.sc.write(":OUTPUT OFF")
    def close(self):
        self.sc.close()
    def getVoltage(self):
        v = self.read()
        if not "," in v: return -999.
        return float(v.split(",")[0])
    def getCurrent(self):
        v = self.read()
        return float(v.split(",")[1])
    def userCmd(self,cmd):
        print "userCmd: %s" % cmd
        return self.sc.writeAndRead(cmd)
    def read(self):
        return self.sc.writeAndRead(":READ?")
    def reset(self):
        self.sc.write("*RST")
\end{lstlisting}


\subsection{Current vs Voltage scan (with signal handler) \& Save to file}
\subsection{Plot the above IV scan in PyROOT}
\subsection{Integrate scan results plot with Elog \& submit IV plot to Elog}
\subsection{Remote access via SSH and Screen}
SSH from personal computer to cern desktop, create Screen, enter it, start IV scan. Resume screen on cern desktop
\subsection{setup.sh file preparation}
A setup.sh file is a bash script used to modify the environment of a terminal to a particular configuration. This is useful to standardise the terminal experience for all users - provided that each user runs the setup.sh script using the following command: \textit{source setup.sh}. It ensures that the same versions of, say Python, or ROOT run for all users who have executed this script and prevents as a result, failures due to version incompatibilities.

An example of the content of a setup.sh file can be seen below.

\begin{lstlisting}
#!/bin/bash
ROOTVER=6.06.04  
GCCVER=4.8 
PYTHONVER=2.7.4 

export CMTCONFIG="x86_64-slc6-gcc48-opt"
export MYPATH=/afs/cern.ch/work/a/adecmos/public/Laser

source /afs/cern.ch/sw/lcg/app/releases/ROOT/$ROOTVER/$CMTCONFIG/root/bin/thisroot.sh 
source /afs/cern.ch/sw/lcg/external/gcc/$GCCVER/$CMTCONFIG/setup.sh 

export PATH=/afs/cern.ch/sw/lcg/external/Python/$PYTHONVER/$CMTCONFIG/bin:$PATH 
export LD_LIBRARY_PATH=/afs/cern.ch/sw/lcg/external/Python/$PYTHONVER/$CMTCONFIG/lib:$LD_LIBRARY_PATH 
export PYTHONPATH=/afs/cern.ch/sw/lcg/app/releases/ROOT/$ROOTVER/$CMTCONFIG/root/lib:$PYTHONPATH 

source $MYPATH/CMT/v*/mgr/setup.sh 

export CMTPATH=$MYPATH 
export PATH=$MYPATH/LaserTCT/python:$PATH
export PATH=$MYPATH/installed/$CMTCONFIG/bin:$PATH 
export PATH=$MYPATH/installed/share/bin:$PATH 
export LD_LIBRARY_PATH=$MYPATH/installed/$CMTCONFIG/lib:/usr/local/lib:$LD_LIBRARY_PATH 
export PYTHONPATH=$MYPATH/pyserial:$PYTHONPATH
export PYTHONPATH=$MYPATH/PySerialComm/python:$PYTHONPATH
export PYTHONPATH=$MYPATH/PyAlarm/python:$PYTHONPATH
\end{lstlisting}

\subsection{Archive code onto a Gitlab repository}

\begin{comment}
Command line instructions

\begin{itemize}
\item \textit{git status} - displays what files have changed or been added or deleted.
\textit{git add -A} - adds all changes ("stages" the files). The A flag adds all changes.
\item \textit{git commit -m "your message"} - commits all changes and adds a message describing this change.
\item \textit{git push} - pushes your changes to the remote repository.
Here's a very useful cheat sheet with multiple basic git commands.
Synchronizing Changes

You may do this in a couple of ways. You can use git fetch in combination with git merge or just git pull. The recommended method is just to do git pull. Here's a description of what they do:
\item \textit{git fetch} - gets all the changes from the repository but doesn't change your local files.
\item \textit{git merge} - combines the changes that you just fetched to update your files.
\item \textit{git pull} - downloads all the changes and updates your local files.
Other
\item \textit{rm -rf .git/} - to remove that directory from “version control”.
\end{itemize}
\end{comment}

\subsection{CMAKE package preparation}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bibliography
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\bibliographystyle{unsrt}
\bibliography{StudentFastTrack}

\end{document}

